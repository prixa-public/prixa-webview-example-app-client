/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, ActivityIndicator} from 'react-native';

import WebView from 'react-native-webview';

const DUMMY_URL = {
  baseUrl: 'https://dev-nalar-3.prixa.ai/?pId=',
  pId: '<Partner_ID>', //e.g. 'ed88cdd0-6200-11eb-a5b1-391be92fc532'
  appId: '<App_ID>', //e.g. '635feb80-6201-11eb-81de-854730b8fb66'
  authToken: '<Auth_Token>', //e.g: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiI2MzVmZWI4MC02MjAxLTExZWItODFkZS04NTQ3MzBiOGZlNjAiLCJzdWIiOiIyZmU0MTMzMC03Mjg2LTExZWItOGQzZC1jNTk5YWJlMWIxNTYifQ.TNvhT3ZewmZapdf0qu6hFzunAWRyUmIXezq2GjC5RT0'
  user_id: '<user_id>', //e.g. '2fe41330-7286-11eb-8d3d-c599abe1c150'
};
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {isVisible: true};
  }

  showLoader() {
    this.setState({isVisible: true});
  }

  hideLoader() {
    this.setState({isVisible: false});
  }

  render() {
    return (
      <SafeAreaView
        renderToHardwareTextureAndroid={true}
        style={
          this.state.isVisible === true ? styles.stylBefore : styles.styleAfter
        }>
        {this.state.isVisible ? (
          <ActivityIndicator
            color="blue"
            size="large"
            style={styles.activityIndicatorStyle}
          />
        ) : null}
        <WebView
          allowsInlineMediaPlayback={true}
          cacheEnabled={true}
          geolocationEnabled={false}
          javaScriptEnabledAndroid={true}
          mediaPlaybackRequiresUserAction={false}
          mixedContentMode={'compatibility'}
          originWhitelist={['*']}
          scalesPageToFit
          startInLoadingState={true}
          useWebkit
          userAgent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36"
          androidHardwareAccelerationDisabled={true}
          style={styles.webViewStyle}
          source={{
            uri:
              DUMMY_URL.baseUrl +
              DUMMY_URL.pId +
              '&appId=' +
              DUMMY_URL.appId +
              '&auth=' +
              DUMMY_URL.authToken,
          }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          onLoadStart={() => this.showLoader()}
          onLoad={() => this.hideLoader()}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  stylBefore: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleAfter: {
    flex: 1,
  },
  webViewStyle: {
    opacity: 0.99,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 40,
  },
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});
