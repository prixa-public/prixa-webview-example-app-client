# prixa-webview-example-app

Prixa Webview Example App using React Native

## Getting started

- [ ] 1. npx react-native init <projectName> --template react-native@^0.64.0

Example: npx react-native init MyAwesomeProject --template react-native@^0.64.0

- [ ] 2. Then, do a project-wide find and replace: com.awesomeproject → com.mycompany.myapp
- [ ] 3. Then, just modify the folder structure to reflect the new package name:
     src/main/java/com/awesomeproject → src/main/java/com/mycompany/myapp
- [ ] cd <projectName>
- [ ] npx react-native-macos-init //for development in macOs
- [ ] navigating to your app.json and lowercasing the name field. Example : "name": “PrixaWeb” into "name": “prixaweb”,
- [ ] npx react-native run-macos //To run macOS app
- [ ] App Will run and show on desktop
- [ ] npx react-native run-ios //To run IOS app
- [ ] macOS dev dependencies
  - [ ] sudo gem install cocoa pods
  - [ ] brew install node
  - [ ] brew install watchman
- [ ] M1 MacBook Related issues - [ ] After executing npx react-native run-ios if error happen then - [ ] Open file <MyProjectName>.xcworkspace in Xcode app - [ ] In the Xcode app select <MyProjectName> in the side bar and select <MyProjectName> in the Project Sidebar and tab Build Settings - [ ] In Architecture section select Exclude Architecture and add arm64
      XCode 13 Build Error - Could not find or use auto-linked library 'swift_Concurrency' - React Native 0.64-65
- [ ] Mac dev Related issues

  - [ ] XCode 13 Build Error - Could not find or use auto-linked library 'swift_Concurrency' - React Native 0.64-65
    - [ ] Solution open file /ios/Podfile and comment out use_flipper!() into #use_flipper!() and run pod install again inside /ios folder
    - [ ] Execute npx react-native run-ios again
  - [ ] [javascript] Invariant Violation: "PrixaWebViewExampleApp" has not been registered.
    - [ ] Solution: change <MyProjectName> in app.json file Example : "name": “prixawebexampleapp” into "name": “PrixaWebViewExampleApp”.
  - [ ] Error Signing for "PrixaWebViewExampleApp" requires a development team. Select a development team in the Signing & Capabilities editor.
    - [ ] If we want to run in real device not simulator we need to add signing profile
      - [ ] Select TARGETS -> AppName
      - [ ] Select Signing & Capabilities Tab
      - [ ] Select Your Team
      - [ ] Rebuild and Run Project by selecting real device not simulator
  - [ ] Error ld: library not found for -lDoubleConversion clang: error: linker command failed with exit code 1 (use -v to see invocation)
    - [ ] We need to add this code below into Podfile below

```
  pod 'React', :path => '../node_modules/react-native/'
  pod 'React-Core', :path => '../node_modules/react-native/'
  pod 'React-Core/DevSupport', :path => '../node_modules/react-native/'
  pod 'React-RCTActionSheet', :path => '../node_modules/react-native/Libraries/ActionSheetIOS'
  pod 'React-RCTAnimation', :path => '../node_modules/react-native/Libraries/NativeAnimation'
  pod 'React-RCTBlob', :path => '../node_modules/react-native/Libraries/Blob'
  pod 'React-RCTImage', :path => '../node_modules/react-native/Libraries/Image'
  pod 'React-RCTLinking', :path => '../node_modules/react-native/Libraries/LinkingIOS'
  pod 'React-RCTNetwork', :path => '../node_modules/react-native/Libraries/Network'
  pod 'React-RCTSettings', :path => '../node_modules/react-native/Libraries/Settings'
  pod 'React-RCTText', :path => '../node_modules/react-native/Libraries/Text'
  pod 'React-RCTVibration', :path => '../node_modules/react-native/Libraries/Vibration'
  pod 'React-Core/RCTWebSocket', :path => '../node_modules/react-native/'
  pod 'React-cxxreact', :path => '../node_modules/react-native/ReactCommon/cxxreact'
  pod 'React-jsi', :path => '../node_modules/react-native/ReactCommon/jsi'
  pod 'React-jsiexecutor', :path => '../node_modules/react-native/ReactCommon/jsiexecutor'
  pod 'React-jsinspector', :path => '../node_modules/react-native/ReactCommon/jsinspector'
  pod 'Yoga', :path => '../node_modules/react-native/ReactCommon/yoga'

  pod 'React-CoreModules', :path => '../node_modules/react-native/React/CoreModules'
  pod 'RCTTypeSafety', :path => "../node_modules/react-native/Libraries/TypeSafety"
  pod 'FBLazyVector', :path => "../node_modules/react-native/Libraries/FBLazyVector"
  pod 'RCTRequired', :path => "../node_modules/react-native/Libraries/RCTRequired"
  pod 'ReactCommon/turbomodule/core', :path => "../node_modules/react-native/ReactCommon"
  pod 'React-perflogger', :path => '../node_modules/react-native/ReactCommon/reactperflogger'

  pod 'DoubleConversion', :podspec => '../node_modules/react-native/third-party-podspecs/DoubleConversion.podspec'
  pod 'glog', :podspec => '../node_modules/react-native/third-party-podspecs/glog.podspec'
  pod 'RCT-Folly', :podspec => '../node_modules/react-native/third-party-podspecs/RCT-Folly.podspec'
```

- [ ] For M1 MacBook you need to set Xcode to launch using Rosetta mode
- [ ] For XCode Version 13.0 (13A233), added libswiftWebKit.tbd to Link Binary with Libraries (from project, Build Phases)
- [ ] For IOS you have to add the following permissions in Info.plist file

```
  <key>NSCameraUsageDescription</key>
  <string>Take pictures for certain activities</string>
  <key>NSPhotoLibraryUsageDescription</key>
  <string>Select pictures for certain activities</string>
  <key>NSMicrophoneUsageDescription</key>
  <string>Need microphone access for recording videos</string>
  <key>NSPhotoLibraryAddUsageDescription</key>
  <string>Save pictures for certain activities.</string>
```
